#include <iostream>
#include <sqlite3.h>
#include <string>
#include <unordered_map>
#include <vector>
#include <sstream>
using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

const char* isav()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				return it->first.c_str();
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				return it->second.at(i).c_str();
		}
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	auto iter = results.end();
	std::stringstream s;
	clearTable();
	s << "select balance from accounts where id =" << from;
	string str = s.str();
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	iter = results.begin();
	int balance = atoi(iter->second.at(0).c_str());
	int rest = balance - amount;
	if (rest >= 0)
	{
	

		clearTable();
		s.str(std::string());
		s << "select balance from accounts where id =" << to;
		str = s.str();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		iter = results.begin();
		int toBal = atoi(iter->second.at(0).c_str());

		s.str(std::string());
		s << "begin transaction";
		str = s.str();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		clearTable();
		s.str(std::string());
		s << "update accounts set balance = " << rest << " where id =" << from << " and balance =" << balance;
		str = s.str();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		clearTable();
		s.str(std::string());
		s << "update accounts set balance = " << (toBal + amount) << " where id =" << to;
		str = s.str();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		s.str(std::string());
		s << "end transaction";
		str = s.str();
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		return true;
		



	}


}
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	auto iter = results.end();
	std::stringstream s;
	clearTable();	
	s << "select available from cars where id =" << carid;
	string str = s.str();
	clearTable();
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	else
	{
		iter = results.begin();
		int av = atoi(iter->second.at(0).c_str());
		if (av ==1)
			{
				clearTable();
				s.str(std::string());
				s << "select balance from accounts where Buyer_id =" << buyerid;
				str = s.str();
				rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
				if (rc != SQLITE_OK)
				{
					cout << "SQL error: " << zErrMsg << endl;
					sqlite3_free(zErrMsg);
					system("Pause");
					return 0;
				}
				else
				{
					iter = results.begin();
					int balance = atoi(iter->second.at(0).c_str());
					clearTable();
					s.str(std::string());
					s << "select price from cars where id =" << carid;
					str = s.str();
					rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
					if (rc != SQLITE_OK)
					{
						cout << "SQL error: " << zErrMsg << endl;
						sqlite3_free(zErrMsg);
						system("Pause");
						return 0;
					}
					else
					{
						iter = results.begin();
						int price = atoi(iter->second.at(0).c_str());
						clearTable();
						if (balance >= price)
						{
							clearTable();
							int rest = balance - price;
							s.str(std::string());
							s << "begin transaction";
							str = s.str();
							rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
							if (rc != SQLITE_OK)
							{
								cout << "SQL error: " << zErrMsg << endl;
								sqlite3_free(zErrMsg);
								system("Pause");
								return 0;
							}
							else
							{
								s.str(std::string());
								s << "update accounts set balance =" << rest << " where id = " << buyerid;
								str = s.str();
								rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
								s.str(std::string());
								s << "update cars set available =" << 2 << " where id =" << carid;
								str = s.str();
								rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
								s.str(std::string());
								s << "end transaction";
								str = s.str();
								rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
								if (rc != SQLITE_OK)
								{
									cout << "SQL error: " << zErrMsg << endl;
									sqlite3_free(zErrMsg);
									system("Pause");
									return 0;
								}
								return true;
							}
						}

					}
			}
		}
		else{
			return 0;
		}
	}
}



int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	flag = carPurchase(1, 4, db, zErrMsg);
	if (flag)
	{
		balanceTransfer(1, 2, 50000, db, zErrMsg);
	}
}